package com.csg.marksman.core

/**
 * Created by allen on 18/6/1.
 */
data class ResultForListDTO<T>(var statusCode: Int = 200, var message: String = "成功", var data: T? = null, var total: Long= 0)