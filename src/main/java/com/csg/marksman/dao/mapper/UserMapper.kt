package com.csg.marksman.dao.mapper

import com.csg.marksman.dao.provider.UserProvider
import com.csg.marksman.model.UserDO
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.SelectProvider
import org.mapstruct.Named

/**
 * Created by allen on 18/6/1.
 */
@Mapper
@Named("UserMapper")
interface UserMapper{
    @SelectProvider(type = UserProvider::class, method = "getItem")
    fun getItem(params: Map<String, Any>): UserDO?
}