package com.csg.marksman.dao.provider

import org.apache.ibatis.jdbc.SQL
import org.slf4j.LoggerFactory

/**
 * Created by allen on 18/6/1.
 */
class UserProvider {
    private val logger = LoggerFactory.getLogger(UserProvider::class.java)
    object Constants {
        const val tableName = "`user`"
        const val id = "`id`"
        const val username = "`username`"
        const val password = "`password`"
        const val phoneNumber = "`phone_number`"
        const val createdAt = "`created_at`"
    }

    fun getItem(params: Map<String, Any>): String {
        return object :SQL() {
           init {
//               SELECT("${Constants.id}," +
//                       "${Constants.username}," +
//                       "${Constants.password}," +
//                       "${Constants.phoneNumber}," +
//                       Constants.createdAt
//               )
//               FROM(Constants.tableName)
//               WHERE("${Constants.phoneNumber} = #{phoneNumber}")
               SELECT("*")
               FROM(Constants.tableName)
               WHERE("id = 1")
           }
        }.toString()
    }
}