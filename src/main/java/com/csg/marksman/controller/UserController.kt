package com.csg.marksman.controller

import com.csg.marksman.core.ResultDTO
import com.csg.marksman.model.UserDO
import com.csg.marksman.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Created by allen on 18/6/1.
 */
@RestController
@RequestMapping("/v1/user")
class UserController {
    private val logger = LoggerFactory.getLogger(UserController::class.java)
    @Autowired
    lateinit var userService: UserService


    @GetMapping("")
    fun getUser(
            @RequestParam("userId", required = false) userId: String): ResultDTO<Any>? {
        val value = userService.getItem(userId)
        return ResultDTO(data = value)
    }

    @PostMapping("")
    fun insertUser(
            @RequestBody user: UserDO
    ): ResultDTO<Any> {
        userService.insert(user)
        return ResultDTO(data = "success")
    }
}