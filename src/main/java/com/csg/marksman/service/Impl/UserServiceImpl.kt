package com.csg.marksman.service.Impl

import com.csg.marksman.model.UserDO
import com.csg.marksman.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service
import javax.annotation.Resource

/**
 * Created by allen on 18/6/1.
 */
@Service
class UserServiceImpl: UserService {

    @Resource
    lateinit var redisTemplate: RedisTemplate<String, String>

    @Autowired
    override fun getItem(id: String): String? {
        if (id.isEmpty()) return null
        val result = redisTemplate.opsForValue().get(id)
        println("test: $result")
        return result
    }

    override fun insert(user: UserDO) {
        val name = user.username
        val password = user.password
        redisTemplate.opsForValue().set(name, password)
        println("password: ${ redisTemplate.opsForValue().get(name)}")
    }

}