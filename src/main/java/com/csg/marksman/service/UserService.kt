package com.csg.marksman.service

import com.csg.marksman.model.UserDO

/**
 * Created by allen on 18/6/1.
 */
interface UserService {
    fun getItem(phoneNumber: String): String?

    fun insert(user: UserDO)
}