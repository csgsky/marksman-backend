package com.csg.marksman.model

import java.util.*

/**
 * Created by allen on 18/6/1.
 */
data class UserDO(
    var id: Long? = null,
//    var name: String? = null,
//    var age: String? = null,
//    var token: String? = null
    var username: String = "",
    var password: String = "",
    var phoneNumber: String = "",
    var createAt: Date? = null
)